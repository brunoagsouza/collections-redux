import { PAGE_SELECTOR } from '../actions/page';

const defaultState = { page: true }

function reducer(state = defaultState, action) {
  const page = !state.page
  switch (action.type) {
    case PAGE_SELECTOR:
      return {
        ...state,
        page: page
      };

    default:
      return state;
  }
}
export default reducer;