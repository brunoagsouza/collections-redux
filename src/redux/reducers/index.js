import { combineReducers } from "redux";
import collection from "./collection-reducer";
import page from "./page";
import charListChanger from "./char-list-changer";

export default combineReducers({ collection, page, charListChanger });
