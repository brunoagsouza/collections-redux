import { ADD_CHAR, DATA_DELETE } from '../actions/collections';

const collectionData = JSON.parse(localStorage.getItem("collection"));

const defaultState = { collection: (collectionData ? collectionData : []) };

function reducer(state = defaultState, action) {

  switch (action.type) {
    case ADD_CHAR:
      return {
        ...state,
        collection: [...state.collection, action.newChar]
      };

    case DATA_DELETE:
      return {
        ...state,
        collection: []
      }

    default:
      return state;
  }

}

export default reducer;