import { CHAR_LIST } from '../actions/charlist.js';

const defaultState = { newCharList: [] }

function reducer(state = defaultState, action) {
  switch (action.type) {
    case CHAR_LIST:
      return {
        ...state,
        newCharList: action.newCharList
      };

    default:
      return state;
  }
}
export default reducer;