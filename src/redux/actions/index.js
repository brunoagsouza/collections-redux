import { ADD_CHAR, DATA_DELETE } from "./collections.js";
import { PAGE_SELECTOR } from "./page.js";
import { CHAR_LIST } from "./charlist.js"

export const addChar = (newChar) => {
  return {
    type: ADD_CHAR,
    newChar: newChar
  }
}
export const collectionDelete = () => {
  return {
    type: DATA_DELETE
  }
}
export const changePage = () => {
  return {
    type: PAGE_SELECTOR
  }
}
export const charListChanger = (newCharList) => {
  return {
    type: CHAR_LIST,
    newCharList: newCharList
  }
}