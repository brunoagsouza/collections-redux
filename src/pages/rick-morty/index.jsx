import React, { useState, useEffect } from 'react';
import CharList from '../../components/charlist';
import { Button } from 'antd';
import styled from "styled-components";
import { useHistory, useParams } from "react-router-dom";
import axios from 'axios';
import ChangeButton from '../../components/change-list';
import { HiSwitchHorizontal } from 'react-icons/hi';
import { useDispatch } from "react-redux";
import { charListChanger } from "../../redux/actions/"
function RmPage() {
  let { page } = useParams();
  const [rmInfo, setRmInfo] = useState({});
  const [pageAtual, setPage] = useState(page);
  const dispatch = useDispatch();
  let history = useHistory();
  useEffect(() => {
    dispatch(charListChanger([]))
    axios.get("https://rickandmortyapi.com/api/character/?page=" + page)
      .then((res) => {
        dispatch(charListChanger(res.data.results));
        setRmInfo(res.data.info);
      })
  },
    [dispatch, page, pageAtual]
  )
  return (
    <div>
      <H1>Rick & Morty</H1>
      <ButtonContainer><ChangeButton>
        <H1><HiSwitchHorizontal /></H1>
      </ChangeButton>
      </ButtonContainer>
      {rmInfo && <CharList type="rick" />}
      {!rmInfo && <h1>Loading...</h1>}
      {rmInfo && <Page>{pageAtual}</Page>}
      {rmInfo &&
        <ButtonContainer>
          {page > 1 && <Button onClick={() => { setPage(parseInt(pageAtual) - 1); history.push("/characters/" + (parseInt(page) - 1)) }}>Previous</Button>}
          {page < parseInt(rmInfo.pages) && <Button onClick={() => { setPage(parseInt(pageAtual) + 1); history.push("/characters/" + (parseInt(page) + 1)) }}>Next</Button>}
        </ButtonContainer>
      }
    </div>
  )
}

export default RmPage;

const ButtonContainer = styled.div`
  display:flex;
  flex-flow: row wrap;
  justify-content:space-around;
  margin: 5vh auto 1vh;

`
const Page = styled.h3`
text-align: center;
margin-top: 5vh`
const H1 = styled.h1`
text-align: center;
margin-top: 5vh`