import React, { useState, useEffect } from 'react';
import CharList from '../../components/charlist';
import { Button } from 'antd';
import styled from "styled-components";
import { useHistory, useParams } from "react-router-dom";
import axios from 'axios';
import ChangeButton from '../../components/change-list';
import { HiSwitchHorizontal } from 'react-icons/hi';
import { useDispatch } from "react-redux";
import { charListChanger } from "../../redux/actions/";

function PokePage({ collectionAdd, changeCharacter }) {
  let { page } = useParams();
  const [pokeInfo, setpokeInfo] = useState(undefined);
  const [pageAtual, setPage] = useState(page);
  let history = useHistory();
  const dispatch = useDispatch();

  const EndPointConstructor = (pagination) => {
    return ("https://pokeapi.co/api/v2/pokemon?offset=" + (20 * (pagination - 1)) + "&limit=20")
  }

  useEffect(() => {
    axios.get(EndPointConstructor(page))
      .then((res) => {
        dispatch(charListChanger(res.data.results));
        setpokeInfo({ count: res.data.count, next: res.data.next, previous: res.data.previous })
      })
  },

    [dispatch, page, pageAtual]
  )
  return (
    <div>
      <H1>Pokémon</H1>
      <ButtonContainer><ChangeButton changeCharacter={changeCharacter}>
        <H1><HiSwitchHorizontal /></H1>
      </ChangeButton></ButtonContainer>
      {pokeInfo && <CharList type="poke" />}
      {!pokeInfo && <h1>Loading...</h1>}
      {pokeInfo && <Page>{pageAtual}</Page>}
      {pokeInfo &&
        <ButtonContainer>
          {pokeInfo.previous && <Button onClick={() => { setPage(parseInt(pageAtual) - 1); history.push("/characters/" + (parseInt(page) - 1)) }}>Previous</Button>}
          {pokeInfo.next && <Button onClick={() => { setPage(parseInt(pageAtual) + 1); history.push("/characters/" + (parseInt(page) + 1)) }}>Next</Button>}
        </ButtonContainer>
      }
    </div>
  )
}

export default PokePage;

const ButtonContainer = styled.div`
  display:flex;
  flex-flow: row wrap;
  justify-content:space-around;
  margin: 5vh auto 1vh;

`
const Page = styled.h3`
text-align: center;
margin-top: 5vh`
const H1 = styled.h1`
text-align: center;
margin-top: 5vh;`

