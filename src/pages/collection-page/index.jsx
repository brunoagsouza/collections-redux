import React, { useState } from 'react';
import CharList from '../../components/charlist';
import styled from "styled-components";
import filtered from "../../components/filter";
import { Input, Radio } from 'antd';
import { useDispatch, useSelector } from "react-redux";
import { collectionDelete } from "../../redux/actions"


function CollectionPage() {
  const [filtering, setFilter] = useState("");
  const { Search } = Input;
  const dispatch = useDispatch();

  let myCollection = useSelector((state) => state.collection.collection)

  return (
    <div>
      <H1>Char Collection</H1>
      {((myCollection.length !== 0) || filtering) &&
        <Filter>
          <Filter>
            <button onClick={() => { dispatch(collectionDelete()) }}>Delete Collection</button>
          </Filter>
          <Filter>
            <Search
              placeholder="input char name"
              onSearch={value => setFilter(value)}
              style={{ width: 200 }}
            />
          </Filter>
          <Filter>
            <Radio.Group onChange={(e) => setFilter(e.target.value)} value={filtering}>
              <Radio value={""}>Todos</Radio>
              <Radio value={"Rick and Morty"}>Rick and Morty</Radio>
              <Radio value={"Pokémon"}>Pokémon</Radio>
            </Radio.Group>
          </Filter>
        </Filter>
      }
      {myCollection && <CharList type="collection" filtred={filtered(myCollection, filtering)} />}
      {myCollection.length === 0 && <H1>Nenhum Personagem Adcionado</H1>}
    </div>
  )
}

export default CollectionPage;

const H1 = styled.h1`
text-align: center;
margin-top: 5vh;`
const Filter = styled.div`
text-align: center;
margin: 2vh auto;`
