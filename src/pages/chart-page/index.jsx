import React from "react";
import { Pie } from "react-chartjs-2";
import styled from "styled-components";
import { useSelector } from "react-redux";

const Chart = () => {

  const myCollection = useSelector((state) => state.collection.collection);

  const charactersData = myCollection.reduce((current, { type }) => {
    current[type] ? (current[type] += 1) : (current[type] = 1);
    return current;
  }, {});

  const data = {
    labels: Object.keys(charactersData),
    datasets: [
      {
        data: Object.values(charactersData),
        backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
        hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
      },
    ],
  };
  return (
    <div>

      <H1> Gráfico myCollections </H1>

      {myCollection.length === 0 && <H1>No Data</H1>}
      {data !== null && data !== undefined && <Pie data={data} />}

    </div>
  );
};

export default Chart;

const H1 = styled.h1`
text-align: center;
margin-top: 5vh;
margin-bottom: 5vh;
`