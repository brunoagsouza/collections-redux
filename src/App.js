import React from 'react';
import './App.css';
import { Div, CenterMenu } from './components/styled-layout';
import { Layout } from 'antd';
import Routes from './components/routes';
import { Link, useLocation } from 'react-router-dom';
import { BiHome } from 'react-icons/bi';
import { BsPieChart } from 'react-icons/bs';
import { CgGhostCharacter } from 'react-icons/cg';
import { useSelector } from "react-redux";

const { Header, Content, Footer } = Layout;

function App() {

  const { pathname } = useLocation();

  const charactersTest = (pathname.indexOf("characters") !== -1);
  const chartTest = (pathname.indexOf("chart") !== -1);

  localStorage.setItem("collection", JSON.stringify(useSelector((state) => state.collection.collection)));

  return <Div id="APP">
    <Layout>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100vw', padding: 0, height: "fit-content" }}>
        <div className="logo" />
        <CenterMenu theme="dark" mode="horizontal" defaultSelectedKeys={['']}>
          <CenterMenu.Item
            key="1"
            className={!(charactersTest || chartTest) && "ant-menu-item ant-menu-item-only-child ant-menu-item-selected"}>
            <Link to="/"><BiHome /> Home</Link>
          </CenterMenu.Item>
          <CenterMenu.Item key="2"
            className={((charactersTest) && "ant-menu-item ant-menu-item-only-child ant-menu-item-selected")}
          >
            <Link to="/characters/1"><CgGhostCharacter /> Char's </Link>
          </CenterMenu.Item>
          <CenterMenu.Item key="3"
            className={(chartTest) && "ant-menu-item ant-menu-item-only-child ant-menu-item-selected"}>
            <Link to="/charts"><BsPieChart />Chart</Link></CenterMenu.Item>
        </CenterMenu>
      </Header>
      <Content className="site-layout" style={{ padding: '0', marginTop: 64, width: "95vw" }}>
        <div className="site-layout-background" style={{ padding: 0, minHeight: '100vh', width: '100%', margin: '0px', boxSizing: "border-box" }}>
          <Routes />
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>Bruno Alexandre ©2020</Footer>
    </Layout>
  </Div >


}

export default App;

