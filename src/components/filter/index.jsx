const filterCollection = (collection = [], filtering = "") => {

  return collection.filter((char) => {

    if (((char.type === filtering) || (char.name.includes(filtering)))) {
      return true;
    }
    return false;
  })

}

export default filterCollection;
