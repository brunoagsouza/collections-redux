import React from 'react';
import { Switch, Route } from "react-router-dom";
import RickMorty from "../../pages/rick-morty";
import PokePage from "../../pages/pokemon-page";
import CollectionPage from "../../pages/collection-page";
import ChartPage from "../../pages/chart-page";
import { useSelector } from 'react-redux';


function Routes() {
  const pageSwitch = useSelector((state) => state.page.page);

  return (<Switch>
    <Route exact path="/">
      <CollectionPage />
    </Route>

    <Route exact path="/characters/:page">
      {pageSwitch && <RickMorty />}
      {!pageSwitch && <PokePage />}
    </Route>

    <Route exact path="/charts">
      <ChartPage />
    </Route>


  </Switch>)


}
export default Routes