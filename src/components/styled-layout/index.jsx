import styled from 'styled-components';
import { Menu } from 'antd';


export const Div = styled.div`
  box-sizing: border-box;
  margin: 0 auto;
  min-height: 100vh;
`
export const CenterMenu = styled(Menu)`
  box-sizing: border-box;
  width: fit-content;
  margin: 0 auto;
  padding: 0px;
`