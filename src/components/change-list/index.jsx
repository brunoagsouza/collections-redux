import React from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { changePage } from '../../redux/actions';

const ChangeCharList = ({ children }) => {

  const dispatch = useDispatch();

  function change() {
    dispatch(changePage())
  }
  return (
    <ChangeButton onClick={() => { change() }}>{children}</ChangeButton>
  )

}
export default ChangeCharList;

const ChangeButton = styled.button`
  background-color: transparent;
  border: none;
  padding: 0;
  margin:0;
  height:100%;
  min-width: 10vh;
`