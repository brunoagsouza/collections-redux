import React from 'react';
import CharCard from '../char-card';
import styled from 'styled-components';
import { useSelector } from "react-redux";

function CharList({ type, filtred }) {

  const chars = useSelector((state) => state.charListChanger.newCharList);

  if (type === "poke") {

    return (<Div>{chars.map(function (char) {
      const id = char.url.split("/", 7);
      return (<CharCard name={char.name}
        image={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + id[6] + ".png"}
        type="Pokémon"
        key={char.name} />)
    }

    )}</Div>);

  }
  else if (type === "rick") {

    return (<Div>{chars.map(function (char) {
      return (<CharCard name={char.name}
        image={char.image}
        type="Rick and Morty"
        key={char.name} />)
    }

    )}</Div>);
  }
  else {
    const chars = filtred;
    return (<Div>{chars.map(function (char) {
      return (<CharCard name={char.name}
        image={char.image}
        type={char.type}
        key={char.name} />)
    }

    )}</Div>);
  }

}
const Div = styled.div`
  display:flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  max-width:90vw;
  box-sizing:border-box;
  padding:0px;
  margin: 0 auto;
  `

export default CharList;