import React from 'react';
import { Card } from 'antd';
import { motion } from 'framer-motion';
import styled from 'styled-components';
import 'antd/dist/antd.css';
import { useDispatch, useSelector } from "react-redux";
import { addChar } from "../../redux/actions";
import { notification } from "antd";
const CharCard = ({ name, type, image }) => {
  const InterationAnimation = styled(motion.div)`
  height:fit-content;
`
  const dispatch = useDispatch();
  let myCollection = useSelector((state) => state.collection.collection);

  const addNewChar = (newChar = {}) => {
    let add = myCollection.find((char) => char.image === newChar.image);
    if (add !== undefined) {
      return notification.error({
        key: newChar.name,
        message: "Erro",
        description: "Personagem já está na coleção",
        placement: 'bottomLeft',
        rtl: true,
        style: { maxWidth: "50vw" }
      });
    }
    else {
      notification.success({
        key: newChar.name,
        message: "Nice!",
        description: "Personagem adicionado à coleção",
        placement: 'bottomLeft',
        rtl: true,
        style: { maxWidth: "50vw" }

      });

      dispatch(addChar(newChar));

    }



  }

  return (
    <InterationAnimation
      whileHover={{ scale: 1.1 }}
      whileTap={{ scale: 0.8 }}
      onClick={() => { addNewChar({ name: name, type: type, image: image }) }}
    >
      <Card
        hoverable
        style={{ maxWidth: "240px", width: "80vw", marginTop: "5vh", minHeight: "445px" }}
        cover={<img alt="example" src={image}
        />}
      >
        <h2>{name}</h2>
        <h3>{type}</h3>
      </Card>
    </InterationAnimation>


  );

}


export default CharCard;